from django.shortcuts import render,redirect
from django.http import HttpResponse
from .forms import Schedule
from . import forms
# Create your views here.
def PPW3html(request):
    return render(request, 'PPW3html.html')

def PPW3html2(request):
    return render(request, 'PPW3html2.html')

def PPW3html3(request):
    return render(request, 'PPW3html3.html')

def PPW3html4(request):
    return render(request, 'PPW3html4.html')

def PPW3html5(request):
    return render(request, 'PPW3html5.html')

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'PPW3html7.html', {'schedules': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'PPW3html6.html', {'form': form})

def schedule_delete(request):
	Schedule.objects.all().delete()
	return render(request, 'PPW3html7.html')

def delete(request, delete_id):
	Schedule.objects.filter(id=delete_id).delete()
	return redirect('schedule')


# Create your views here.
