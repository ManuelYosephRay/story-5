from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r"^deleteurl/(?P<delete_id>\d+)/$",views.delete, name='delete'),
    path('', views.PPW3html, name = 'PPW3html'),
    path('experiences/', views.PPW3html2, name = 'PPW3html2'),
    path('education/', views.PPW3html3, name = 'PPW3html3'),
    path('contact/', views.PPW3html4, name = 'PPW3html4'),
    path('warning/', views.PPW3html5, name = 'PPW3html5'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/create', views.schedule_create, name='schedule_create'),
    path('schedule/delete', views.schedule_delete, name='schedule_delete'),

]
